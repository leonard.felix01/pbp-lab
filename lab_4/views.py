from django.shortcuts import render
from .forms import NoteForm
from .models import Note
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    notes =  Note.objects.all().values() # TODO Implement this
    #set notes as a collection of object Notes
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None) #create an object form of NoteForm and put the data in form
    if form.is_valid() and request.method == 'POST':    #check if the data is valid and the request method is post
        form.save() #save the form
        return HttpResponseRedirect('/lab-4')   #return to the index page to display the list

    # request to lab4_form to display the html page and taking the user input
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes =  Note.objects.all().values() # TODO Implement this
    #set notes as a collection of object Notes
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
