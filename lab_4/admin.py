from django.contrib import admin
from .models import Note

# Register your models here.
admin.site.register(Note) #add so from the admin Note objects can be added
