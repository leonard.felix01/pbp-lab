from django import forms
from .models import Note #import Note class from model

class NoteForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Note #specify the model used
        fields = "__all__" #specify the fields of attributes to all attributes in Friend
