from django.db import models

# Create your models here.
class Note(models.Model):
    To = models.CharField(max_length=30)  #add a To attribute as Charfield
    From = models.CharField(max_length=30)   #add a From attribute as Charfield
    Title = models.CharField(max_length=30)    #add a Title attribute as Charfield
    Message = models.TextField()    #add a Message attribute as Charfield
