1.
JSON (JavaScript Object Notation) and XML (Extensible Markup Language) are both data delivery method which is self describing, meaning the syntax is simple and a human being can simply understand the meaning of the data displayed, however there are also some differences between the 2 methods. XML used the tag format, for instance `<dairy>Cheese</dairy>`, to store the information. Meanwhile JSON directly store in format of "dairy": "Cheese" , without the tag. Another difference, XML is structured in a tree like structure(root, stem, leaf), since all XML documents must contain a root element and the other element is the content from the root, example

```
<food>
  <dairy>Cheese</dairy>
</food>
```

in this case food is the root and dairy is kept inside the root, however that isn't the case with JSON, it doesn't need a root element.
```JSON
{
  "dairy": "Cheese"
}
```

2.
*Different* from XML, HTML(Hypertext Markup Language) describes the structure of a webpage where XML describes and store only the data
here are an example of an HTML file
```html
<html>
  <head>
    <title>Food</title>
  </head>
  <body>
    <h1>Cheese</h1>
  </body>
</html>
```

this is clearly different from xml and JSON which the purpose is just as a data container, while html display the entire website not just the data such as head, title, and body. The implementation of XML and JSON are used inside HTML as a data container which will be displayed by the structure described in the html file.

ps.please do open this file with web IDE/edit option, so that the html structure isn't rendered
