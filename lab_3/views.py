from django.shortcuts import render
from .forms import FriendForm
from .models import Friend
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')  #decorator to force the user to login first to be able to acess the method
def index(request):
    friends =  Friend.objects.all() # TODO Implement this
    #set friends as a collection of object Friends
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')  #decorator to force the user to login first to be able to acess the method
def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None) #create an object form of FriendForm and put the data in form
    print(request.POST)
    if form.is_valid() and request.method == 'POST':    #check if the data is valid and the request method is post, (method = GET only take data, no change)
        form.save() #save the form
        return HttpResponseRedirect('/lab-3')   #return to the index page to display the list

    # request to lab3_form to display the html page and taking the user input
    context['form']= form
    return render(request, "lab3_form.html", context)
