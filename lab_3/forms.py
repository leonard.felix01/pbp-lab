from django import forms
from .models import Friend #import Friend class from model
from django.forms.widgets import NumberInput

# create a ModelForm
class FriendForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Friend #specify the model used
        fields = "__all__" #specify the fields of attributes to all attributes in Friend

    #special for npm displays in label, and set the length must be 10
    NPM = forms.CharField(label= 'NPM(Nomor Pokok Mahasiswa) :', min_length = 10, max_length = 10)

    #special for date of birth, ask the user in label, set the input widget to NumberInput for DateField
    DOB = forms.DateField(label='Date Of Birth :', widget=NumberInput(attrs={'type': 'date'}))
