from django.urls import path
from .views import index, add_friend

app_name = "lab_3"
urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add_friends') #url of /add to acess adding friends form page
    # TODO Add friends path using friend_list Views
]
