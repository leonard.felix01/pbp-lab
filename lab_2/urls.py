from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'), #added path index as default
    path('xml', xml, name='xml'), #added path xml to view xml views
    path('json', json, name='json') #added path xml to view xml views
]
