from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    notes =  Note.objects.all().values() # TODO Implement this
    #set notes as a collection of object Notes
    response = {'notes': notes}
    return render(request, 'lab_2.html', response)

def xml(request): #method for xml file viewing
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request): #method for json file viewing
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
