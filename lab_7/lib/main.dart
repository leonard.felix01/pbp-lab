import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Home())
  );
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text('Mutuals'),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: Center(child:
      Column(
        children: <Widget>[SizedBox(
          height: 50,
        ),
          Container(
            color: Color(0xFFFFF385),
            width: 600,
            height: 70,
            child: const Text("Hi, User \n",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 50
              ),
            ),

          ),
          Container(
            color: Color(0xFFFFF385),
            width: 600,
            child: const Text("Here are some of our suggestions \n",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20
              ),
            ),

          ),
          Container(
            color: Color(0xFFFFF385),
            width: 600,
            child: Image.asset('images/header.png'), height: 200,

          ),
          SizedBox(height: 60),

          Container(
            color: Color(0xFFFFFAC9),
            width: 600,
            child: const Text("Cbkadal          Both of you like swimming \n",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20
              ),
            ),

          ),

          SizedBox(height: 20),

          Container(
            color: Color(0xFFFFFAC9),
            width: 600,
            child: const Text("Bob          Both of you like drawing \n",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20
              ),
            ),

          ),

          SizedBox(height: 20),

          Container(
            color: Color(0xFFFFFAC9),
            width: 600,
            child: const Text("Charles          Both of you like singing and chess \n",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20
              ),
            ),

          ),

          SizedBox(height: 20),

          const Text("Still not finding who you're looking for ? \n",
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SecondWindow()),
              );
            },
            child: const Text("Shuffle Mutuals"),
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                primary: Color(0xFFFFF385),
                onPrimary: Colors.black,
                minimumSize: Size(200, 50)


            ),
          )
        ],
      ),
      ),
    );
  }
}

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Image.asset('images/logo.png'),
            decoration: BoxDecoration(
                color: Color(0xFFFFF385),
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/cover.jpg'))),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('About'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Friends'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('User'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}

class SecondWindow extends StatefulWidget {
  @override
  SecondWindowState createState() => new SecondWindowState();
}

class SecondWindowState extends State<SecondWindow> {
  bool alert = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text('Mutuals'),
        centerTitle: true,
        backgroundColor: const Color(0xFFFFF385),
        foregroundColor: Colors.black,
      ),
      body: Center(child: Column(
        children: <Widget>[
          Image.asset('images/logo.png', height: 200,),

          const SizedBox(height: 50),

          const Text('More features are coming soon :)',
              style: TextStyle(
              fontSize: 20
            ),
          ),

          const SizedBox(height: 50),

          IconButton(onPressed:() {
            setState(() {
              alert = !alert;
            });
          }
              , icon: Icon(Icons.add_alert),
          ),

          const SizedBox(height: 20),

          Container(
            child: alert?
              Text("We'll notify you when a new update has been released"):
              Text("")
          ),

          const SizedBox(height: 20),

          ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Return"),
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                primary: Color(0xFFFFF385),
                onPrimary: Colors.black,
                minimumSize: Size(200, 50)
            ),
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),

      )
    );
  }
}





