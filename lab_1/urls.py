from django.urls import path
from .views import index, friend_list

app_name = "lab_1"
urlpatterns = [
    path('', index, name='index_lab1'),
    path('friends', friend_list, name='friend_list') #added path for the url to go to /friends and display the current friend_list
    # TODO Add friends path using friend_list Views
]
