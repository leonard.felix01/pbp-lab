from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    Name = models.CharField(max_length=30)  #add a name attribute as Charfield
    NPM = models.CharField(max_length=30)   #add a NPM attribute as Charfield
    DOB = models.DateField()    #add a date of birth attribute as a date field
    # TODO Implement missing attributes in Friend model
